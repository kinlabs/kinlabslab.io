# kinLabs

Krystahl Intelligence Science & Research Laboratory, Experimental Design
Playground, & Celestial Technology Innovation Development Accelerator.

KinLabs is the Incubator for the tools supported the Krysthal Inteligence Organism via 
an [holoKIT][1] and [holoTools][2]
 

### tools under development are :

* [holoLinks](https://kinlabs.framagit.org/holoLinks)

* [holoApps](https://kinlabs.framagit.org/holoApps)
* [holoBadges](https://kinlabs.framagit.org/holoBadges)
* [holoGIT](https://kinlabs.framagit.org/holoGIT)
* [holoIndexes](https://kinlabs.framagit.org/holoIndexes)
* [holoKeys](https://kinlabs.framagit.org/holoKeys)
* [holoMarks](https://kinlabs.framagit.org/holoMarks)
* [holoNames](https://kinlabs.framagit.org/holoNames)
* [holoServe](https://kinlabs.framagit.org/holoServe)
* [holoTapps](https://kinlabs.framagit.org/holoTapps)



[1]: https://gitlab.com/holokit
[2]: https://gitlab.com/holoverse4/holotools
